declare function require(name: string);

const fs = require('fs');
var jsdom = require('jsdom');

var fileName="test";
var max=6000;
var height = 50;
var width = 50;
let offsetX = 25;
let offsetY = 48; // 94
let skewX = 2;
let skewY = 2;

let dim: number =40;

const {JSDOM} = jsdom;

var htmlFile = fs.readFileSync('tsc.html');
const dom = new JSDOM(htmlFile);

const svgNS = "http://www.w3.org/2000/svg";

let SVG = function (h: number, w: number): Element {
    const aSVG = dom.window.document.createElementNS(svgNS, "svg");
    aSVG.setAttribute('height', h.toString());
    aSVG.setAttribute('width', w.toString());
    aSVG.setAttribute('id', 'aSVG');
    return aSVG;
};

let aSVG = SVG(height * dim, width * dim);
console.log(dom.window.document.getElementById("root").appendChild(aSVG));


function compactArray(a: Array<number>): object {
    var b: object = {};
    for (var i = 0; i < a.length; i++) {
        if (a[i] !== undefined && a[i] != null) {
            b[primes[i]] = a[i];
        }
    }
    return b;
}


function reduceArray(a: Array<number>): object {
    var b: object = {};
    for (var i = 0; i < a.length; i++) {
        if (a[i] !== undefined && a[i] != null) {
            b = {};
            b[i] = a[i];
        }
    }
    return b
}

/*

let circle = function (cx: number, cy: number, r: number) {
    let SVGObj = dom.window.document.createElementNS(svgNS, "circle");
    SVGObj.setAttribute('cx', (offsetX + cx * dim).toString());
    SVGObj.setAttribute('cy', (offsetY + cy * dim).toString());
    SVGObj.setAttribute('r', (r * dim).toString());
    SVGObj.setAttribute('id', cx + ":" + cy);
    let hue = (cx * 236) % 360;
    SVGObj.setAttribute("style", "fill:hsl(" + hue + ",100%,50%); stroke:hsl(" + hue + ",100%,50%)");
    var childNode = aSVG.firstChild;
    aSVG.insertBefore(SVGObj, childNode);
};

*/

let line = function (x1: number, y1: number, x2: number, y2: number): void {
    // console.log("x1: " + x1 + ", y1: " + y1 + ", x2: " + x2 + ", y2: " + y2)
    let SVGObj = dom.window.document.createElementNS(svgNS, "line");
    SVGObj.setAttribute('x1', (-offsetX * dim + x1 * dim).toString());
    SVGObj.setAttribute('y1', (-offsetY * dim + y1 * dim).toString());
    SVGObj.setAttribute('x2', (-offsetX * dim + x2 * dim * skewX).toString());
    SVGObj.setAttribute('y2', (-offsetY * dim + y2 * dim ).toString());
    SVGObj.setAttribute('id', y1 + " = prime " + x2 + " * factor " + y1/x2);
    SVGObj.setAttribute("style", "stroke:rgb(0, 200, 255);");
    aSVG.appendChild(SVGObj);
    let SVGObj2 = dom.window.document.createElementNS(svgNS, "line");

    SVGObj2.setAttribute('x1', (-offsetX * dim + y1 * dim).toString());
    SVGObj2.setAttribute('y1', (-offsetY * dim + x1 * dim).toString());
    SVGObj2.setAttribute('x2', (-offsetX * dim + y2 * dim).toString());
    SVGObj2.setAttribute('y2', (-offsetY * dim + x2 * dim * skewY).toString());
    SVGObj2.setAttribute('id', y1 + " = prime " + x2 + " * factor " + y1/x2);
    SVGObj2.setAttribute("style", "stroke:rgb(0, 255,200);");
    aSVG.appendChild(SVGObj2);
};

// line(20, 20, 200, 200);


let allFactors: Array<number> = [];

var getPrimes = function (myMax: number): number[] {
    let sieve = [],
        i, j, primes: number[] = [];
    for (i = 2; i <= myMax; ++i) {
        if (!sieve[i]) {
            // i has not been marked -- it is prime
            primes.push(i);
            for (j = i < 1; j <= myMax; j += i) {
                sieve[j] = true;
            }
        }
    }
    return primes;
};

var primes = getPrimes(1000000);

//alert(primes);

var getFactors = function (myNum: number, myNumFactors: number[]): number[] {
    for (var myPrimeIndex = 0; myPrimeIndex < primes.length; myPrimeIndex += 1) {
        if (myNum % primes[myPrimeIndex] === 0) {
            if (myNumFactors [myPrimeIndex] === undefined) {
                myNumFactors [myPrimeIndex] = 1;
            } else {
                myNumFactors [myPrimeIndex] = myNumFactors[myPrimeIndex] + 1;
            }
            myNum = myNum / primes[myPrimeIndex];
            if (myNum > 1) {
                getFactors(myNum, myNumFactors)
            }
        }
    }
    return myNumFactors;
};

//console.log('ok');

var i: number;

for (i = 2 ; i < max; i++) {
    //console.log(i);
    var myNumFactors1: Array<number> = [];
    myNumFactors1 = getFactors(i, myNumFactors1);
    //console.log(" myNumFactors1 " + myNumFactors1);
    var myNumFactors: object = reduceArray(myNumFactors1);
    //console.log(" myNumFactors " + JSON.stringify(myNumFactors));
    for (let j in myNumFactors) {
        //console.log("factors :: i " + i + ": j " + primes[myNumFactors[j]]);
        if (i !== primes[j]){
            line(0, i, primes[j], 0);}
    }
}
;

console.log("hi" + i);


const SVGDefs = dom.window.document.createElementNS(svgNS, "defs");
var childNode = aSVG.firstChild;
aSVG.insertBefore(SVGDefs, childNode);

const SVGStyle = dom.window.document.createElementNS(svgNS, "style");
childNode = SVGDefs.firstChild;
SVGDefs.insertBefore(SVGStyle, childNode);


var cssCode = fs.readFileSync('tsc.css', 'utf8')
const textNode = dom.window.document.createTextNode(cssCode)
SVGStyle.appendChild(textNode);

let myHTML = dom.window.document.getElementById("body").innerHTML;

fs.writeFile("generatedFiles/" + fileName + ".svg", myHTML, function (err) {
    if (err) {
        return console.log(err);
    }
    console.log("The svg file '" + fileName + ".svg' was saved!");
});
